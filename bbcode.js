Drupal.wysiwyg.editor.markitup.sets.bbcode = {
  bold: {name:'Bold', key:'B', openWith:'[b]', closeWith:'[/b]', className:'markitup-bold' },
  italic: {name:'Italic', key:'I', openWith:'[i]', closeWith:'[/i]', className:'markitup-italic'},
  underline: {name:'Underline', key:'U', openWith:'[u]', closeWith:'[/u]', className:'markitup-stroke' },
  image: {name:'Picture', key:'P', replaceWith:'[img][![Url]!][/img]', className:'markitup-picture' },
  link: {name:'Link', key:'L', openWith:'[url=[![Url]!]]', closeWith:'[/url]', placeHolder:'Your text to link...', className:'markitup-link' },
  ul: {name:'Bulleted list', openWith:'[list]\n', closeWith:'\n[/list]', className:'markitup-list-bullet' },
  ol: {name:'Numeric list', openWith:'[list=[![Starting number]!]]\n', closeWith:'\n[/list]', className:'markitup-list-numeric'},
  code: {name:'Code', openWith:'[code]', closeWith:'[/code]', className:'markitup-code' },
  quotes: {name:'Quotes', openWith:'[quote]', closeWith:'[/quote]', className:'markitup-quotes' },
  clean: {name:'Clean', className:"clean", replaceWith:function(markitup) { return markitup.selection.replace(/\[(.*?)\]/g, "") }, className:'markitup-clean' },
  preview: {name:'Preview', call:'preview', className:'markitup-preview' }
};
